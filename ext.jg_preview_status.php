<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jg_preview_status_ext
{
	public $settings = array();
	public $name = 'JG Preview Status';
	public $version = '1.0';
	public $description = 'An Expression Engine extension.  Automatically adds a {jg_preview_status} global variable.  The variable is "open|closed" if anyone is logged in.  By default, "{jg_preview_status}" is "open".';
	public $settings_exist = 'n';
	public $docs_url = '';
	
	/**
	 * __construct
	 * 
	 * @access	public
	 * @param	mixed $settings = ''
	 * @return	void
	 */
	public function __construct($settings = '') {
		$this->EE =& get_instance();
		
	}
	
	/**
	 * activate_extension
	 * 
	 * @access	public
	 * @return	void
	 */
	public function activate_extension() {
		$hook_defaults = array(
			'class' => __CLASS__,
			'settings' => '',
			'version' => $this->version,
			'enabled' => 'y',
			'priority' => 10
		);

		$hooks[] = array(
			'method' => 'sessions_end',
			'hook' => 'sessions_end'
		);
		
		foreach ($hooks as $hook) {
			$this->EE->db->insert('extensions', array_merge($hook_defaults, $hook));
		}
	}
	
	/**
	 * update_extension
	 * 
	 * @access	public
	 * @param	mixed $current = ''
	 * @return	void
	 */
	public function update_extension($current = '') {
		if ($current == '' OR $current == $this->version) {
			return FALSE;
		}
		
		$this->EE->db->update('extensions', array('version' => $this->version), array('class' => __CLASS__));
	}
	
	/**
	 * disable_extension
	 * 
	 * @access	public
	 * @return	void
	 */
	public function disable_extension() {
		$this->EE->db->delete('extensions', array('class' => __CLASS__));
	}
	
	/**
	 * settings
	 * 
	 * @access	public
	 * @return	void
	 */
	public function settings() {
		$settings = array();
		
		return $settings;
	}
	
	/**
	 * sessions_end
	 * 
	 * @access	public
	 * @param	channel $channel
	 * @param	array $query_result
	 * @return	array
	 */
	public function sessions_end ($data) {
		/* Set {jg_preview_status} to open|closed if an admin is logged in */
		$status = 'open';
		if ($data->userdata('username')) {
			$status = 'open|closed';
		}
		$this->EE->config->_global_vars['jg_preview_status'] = $status;
	}
}

/* End of file ext.jg_admin_status.php */
/* Location: ./system/expressionengine/third_party/jg_preview_status/ext.jg_preview_status
*
*.php */